brew-downgrade
==============

`brew-downgrade` is a small and extremely simple script aimed at installing an obsolete version of a selected package. Please, take a look at the source for more details.
